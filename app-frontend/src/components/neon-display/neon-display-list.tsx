import NeonDisplayListItem from "./neon-display-list-item"
import { useSelector, useDispatch } from 'react-redux'
import type { AppDispatch, RootState } from '../../store/store'
import LoadingPlaceholder from "../loading-placeholder/loading-placeholder"
import { backendGetDisplays } from "../../store/neon-display-store-slice"
import { useEffect } from 'react'
import { NeonDisplayDto } from "../../app-common/dtos/neon-display-dtos"
import NeonDisplayEdit from "./neon-display-edit"
import { getStyles } from "./style"

const NeonDisplayList = (): JSX.Element => {

    const dispatch = useDispatch<AppDispatch>()
    const neonDisplayStoreState = useSelector((state: RootState) => state.neonDisplayStore)

    useEffect(() => {
        if (!neonDisplayStoreState.items) {
            dispatch(backendGetDisplays())
        }
    }, [dispatch, neonDisplayStoreState.items])

    if (!neonDisplayStoreState.items) {
        return <LoadingPlaceholder />
    }

    const newItem: NeonDisplayDto = {
        id: "",
        name: "New"
    }

    return (
        <div
            css={getStyles()}
        >
            <NeonDisplayEdit
                verb="Create"
                item={newItem}
            />
            <table
            >
                {neonDisplayStoreState.items.map(item =>
                    <NeonDisplayListItem item={item} />
                )}
            </table>
        </div>
    )
}

export default NeonDisplayList