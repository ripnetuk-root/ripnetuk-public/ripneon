const LoadingPlaceholder = (): JSX.Element => {
    return (
        <p>
            LoAdInG...
        </p>
    )
}

export default LoadingPlaceholder;