import { InfoList } from '../../app-common/dtos/entity-dtos'
import { useAppDispatch, useAppSelector } from "../../hooks/redux-hooks";
import { getApiUrl } from "../../http-client/api-http-client";
import { fetchTest, setMessage } from "../../store/teststore/teststore-actions";
import { TestString} from '../../app-common/testcode/testcode'

const Test = () => {
    const dispatch = useAppDispatch();
    const demoState = useAppSelector(state => state.testStore);

    const handleButtonSetMessageClick = () => {
        dispatch(setMessage("New Messagexxxxx fgfffgf yeee"))
    }

    const handleButtonFetchTestClick = () => {
        dispatch(fetchTest())
    }

    const buildInfoListItem = (item: string) => (
        <tr>
            <td>
                {item}
            </td>
        </tr>
    )


    const buildInfoList = (item: InfoList) => (
        <tr>
            <td>
                TITLE: {item.title}
            </td>
            {TestString}
            <td>
                <table
                    className="table table-compact"
                >
                    <tbody>
                        {item.items.map(item => buildInfoListItem(item))}
                    </tbody>
                </table>
            </td>
        </tr>
    )

    return (
        <div>
            <h6 className="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                <span>Demo {demoState.testDto.testMessage}</span>
            </h6>
            <button
                onClick={() => handleButtonSetMessageClick()}
            >Set Message</button>
            <button
                onClick={() => handleButtonFetchTestClick()}
            >Fetch Test</button>
            API URL: {getApiUrl()}
            <div>
                <table
                    className="table table-compact"
                >
                    <tbody>
                        {demoState.testDto.infoLists.map(item => buildInfoList(item))}
                    </tbody>
                </table>
            </div>
        </div>

    )
}

export default Test;