import { TestDto } from '../app-common/dtos/entity-dtos'
import ApiHttpClient from '../http-client/api-http-client';

const ApiService = {
    async getTest(): Promise<TestDto> {
        const outcome = await ApiHttpClient().get<TestDto>("/test").then(response => {
            return response.data
        }).catch(error => {
            return Promise.reject("Error calling api = " + error)
        })

        return outcome
    }
}

export default ApiService