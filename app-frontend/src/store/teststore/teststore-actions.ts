import { AnyAction } from '@reduxjs/toolkit';
import { ThunkAction } from '@reduxjs/toolkit';
import ApiService from '../../service/api-service';
import { RootState } from '../store';
import testStoreSlice from './teststore-slice';

export const setMessage = (message: string): ThunkAction<void, RootState, unknown, AnyAction> => {
    return async (dispatch, getState) => {
        const newTestDto = {
            ...getState().testStore.testDto,
            testMessage: message
        }
        dispatch(testStoreSlice.actions.setTestDto(newTestDto));
    }
}

export const fetchTest = (): ThunkAction<void, RootState, unknown, AnyAction> => {
    return async (dispatch, getState) => {
        await ApiService.getTest().then(response => {
            dispatch(testStoreSlice.actions.setTestDto(response))
        }).catch(error => {
            console.log("Error " + error)
        })
    }
}
