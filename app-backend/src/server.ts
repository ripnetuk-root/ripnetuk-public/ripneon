import http from 'http';
import express, { Express } from "express";
import morgan from "morgan";
import routes from "./routes";

require('dotenv').config();
let envFile = `.env.${process.env.NODE_ENV}`;
require('dotenv').config({ path: envFile })


const router: Express = express();

// Logging
router.use(morgan("dev"));

// Parse request
router.use(express.urlencoded({ extended: false }));
// Takes care of JSON data
router.use(express.json());

// Rules

router.use((req, res, next) => {
    // CORS
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    if (req.method === "OPTIONS") {
        res.header("Access-Control-Allow-Methods", "*");
        return res.status(200).json({});
    }
    next();
});

// Routes

router.use("/", routes);

// Error handling
router.use((req, res, next) => {
    const error = new Error("Not Found");
    return res.status(404).json({ message: error.message });
});

// Server
const httpServer = http.createServer(router);
const PORT: any = process.env.PORT ?? 6060;

httpServer.listen(PORT, () => console.log(`The server is running on port ${PORT}`));