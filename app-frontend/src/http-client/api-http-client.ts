import axios from 'axios'

export const getApiUrl = () => {
    const api_url = process.env["REACT_APP_API_URL"]
    if ( (!api_url) || (api_url === "") ) {
        return "/api"
    }

    return api_url
}
var ApiHttpClient = () => {
    var client = axios.create({
        baseURL: getApiUrl()
    })

    return client;
}

export default ApiHttpClient;
