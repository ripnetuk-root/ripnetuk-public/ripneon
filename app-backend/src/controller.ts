import { TestDto } from '../../app-frontend/src/app-common/dtos/entity-dtos'
import { NeonDisplayDto } from '../../app-frontend/src/app-common/dtos/neon-display-dtos'
import { TestString } from '../../app-frontend/src/app-common/testcode/testcode'

import { Request, Response, NextFunction } from "express";
import { Repo } from './repo';

export const getTest = async (req: Request, resp: Response, next: NextFunction) => {
    const util = require('util');
    const exec = util.promisify(require('child_process').exec);
    const fs = require('fs');

    const testMessage = TestString + "This is a CHANGED dto " + (new Date()).toLocaleDateString() + " " + (new Date()).toLocaleTimeString()
    
    var testDto: TestDto = {
        testMessage: testMessage,
        infoLists: []
    }

    const addList = (title: string, items: string[]) => {
        testDto.infoLists.push({
            title: title,
            items: items
        })
    }

    const { stdout, stderr } = await exec('set')
    addList("set", stdout.split(/\r?\n/))

    return resp.status(200).json(testDto)
}

export const getNeonDisplay = async (req: Request, resp: Response, next: NextFunction) => {
    const res = Repo.neonDisplay.items
    return resp.status(200).json(res)
}

export const putNeonDisplay = async (req: Request, resp: Response, next: NextFunction) => {
    const item: NeonDisplayDto = req.body
    Repo.neonDisplay.update(item)
    return resp.status(200).end()
}

export const postNeonDisplay = async (req: Request, resp: Response, next: NextFunction) => {
    const item: NeonDisplayDto = req.body
    
    const createdItem = Repo.neonDisplay.create(item)
    return resp.status(200).json(createdItem)
}

export const deleteNeonDisplay = async (req: Request, resp: Response, next: NextFunction) => {
    const id = req.params.id

    Repo.neonDisplay.delete(id)
    
    return resp.status(200).end()
}