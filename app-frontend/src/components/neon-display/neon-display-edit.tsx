import { NeonDisplayDto } from "../../app-common/dtos/neon-display-dtos"
import Modal from 'react-modal';
import { useState } from "react";
import { backendPutDisplay, backendPostDisplay } from "../../store/neon-display-store-slice";
import { useDispatch } from 'react-redux'
import type { AppDispatch } from '../../store/store'

interface Props {
    item: NeonDisplayDto
    verb: string // edit or create
}
const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
    },
};

Modal.setAppElement('#root');

const NeonDisplayEdit = (props: Props): JSX.Element => {
    const [isEdit, setIsEdit] = useState(false);
    const [draftItem, setDraftItem] = useState(props.item)
    const dispatch = useDispatch<AppDispatch>()

    const handleChange = (e: any) => {
        setDraftItem({
            ...draftItem,
            [e.target.name]: e.target.value,
        })
    }

    const handleCancel = () => {
        setIsEdit(false)
        setDraftItem(props.item)
    }

    const handleSave = () => {
        if (draftItem.id !== "") {
            dispatch(backendPutDisplay(draftItem))
        } else {
            dispatch(backendPostDisplay(draftItem))
        }
        setIsEdit(false)
    }

    const popup = <Modal
        isOpen={isEdit}
        style={customStyles}
        contentLabel={props.verb} // for screenreaders
    >
        <h2>
            {draftItem.name}
        </h2>

        <form>
            <label>
                Name:
                <input
                    name="name"
                    value={draftItem.name}
                    onChange={handleChange}
                >
                </input>
            </label>
            <button
                type="button"
                onClick={handleCancel}>
                Cancel
            </button>
            <button
                type="button"
                onClick={handleSave}
            >
                Save
            </button>
        </form>
    </Modal>
    return (
        <div>
            <button
                onClick={(e) => { setIsEdit(true) }}
            >
                {props.verb}
            </button>
            {popup}
        </div>
    )
}

export default NeonDisplayEdit