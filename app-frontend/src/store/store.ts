import { configureStore } from '@reduxjs/toolkit';
import testStoreSlice from './teststore/teststore-slice';
import neonDisplayStoreSlice from './neon-display-store-slice';


const store = configureStore(
    {
        reducer: {
            testStore: testStoreSlice.reducer,
            neonDisplayStore: neonDisplayStoreSlice
        }
    }
)

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch;
export default store;