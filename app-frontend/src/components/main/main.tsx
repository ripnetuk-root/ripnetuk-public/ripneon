import Test from "../../components/host/test";
import NeonDisplayList from '../../components/neon-display/neon-display-list';
import { useState } from "react";
import { getStyles } from "./style"

const Main = () => {

    const [onHold, setOnHold] = useState(false);

    if (onHold) {
        return <div>
            <button
                onClick={() => { setOnHold(false) }}
            >
                Unhold!
            </button>
        </div>
    }

    return (
        <div
            css={getStyles()}
        >
            <Test />
            <NeonDisplayList
            />
        </div>
    );
}

export default Main;