import express from "express";
import { getNeonDisplay, getTest, postNeonDisplay, putNeonDisplay, deleteNeonDisplay } from "./controller";
const router = express.Router();

router.get("/test", getTest)

router.get("/neondisplay", getNeonDisplay)
router.post("/neondisplay", postNeonDisplay)
router.put("/neondisplay/:id", putNeonDisplay)
router.delete("/neondisplay/:id", deleteNeonDisplay)

export = router;