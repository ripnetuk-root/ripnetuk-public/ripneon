import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { TestDto } from '../../app-common/dtos/entity-dtos'

export interface TestStoreState {
    testDto: TestDto;
}

const initialTestStoreState: TestStoreState = {
    testDto: {
        testMessage: "Hello world default NEW WORLD",
        infoLists: [
        ]
    }
    
}

const testStoreSlice = createSlice({
    name: 'demo',
    initialState: initialTestStoreState,
    reducers: {
        setTestDto(state, action: PayloadAction<TestDto>) {
            state.testDto = action.payload
        }
    }
})

export default testStoreSlice;
