import { NeonDisplayDto } from "../../app-common/dtos/neon-display-dtos"
import NeonDisplayEdit from "./neon-display-edit";
import { useSelector, useDispatch } from 'react-redux'
import type { AppDispatch, RootState } from '../../store/store'
import { backendDeleteDisplay, setSelectedItem } from "../../store/neon-display-store-slice";

interface Props {
    item: NeonDisplayDto
}

const NeonDisplayListItem = (props: Props): JSX.Element => {
    const dispatch = useDispatch<AppDispatch>()

    const selectedItemState = useSelector((state: RootState) => state.neonDisplayStore.selectedItem)

    const col = (selectedItemState?.id === props.item.id) ? "green" : "red"

    const handleOnClick = () => {
        dispatch(setSelectedItem(props.item))
    }

    const handleDeleteClick = () => {
        dispatch(backendDeleteDisplay(props.item))
    }

    return (
        <tr
            key={props.item.id}
            style={{ "backgroundColor": col }}
            onClick={(e) => handleOnClick()}
        >
            <td>
                {props.item.id} - {props.item.name}
            </td>
            <td>
                <NeonDisplayEdit
                    verb="Edit"
                    item={props.item}
                />
            </td>
            <td>
                <button
                    onClick={() => handleDeleteClick()}
                >
                    Delete
                </button>
            </td>
        </tr>
    )
}

export default NeonDisplayListItem