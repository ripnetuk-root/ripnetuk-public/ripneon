export interface BaseNonDto {
    id: string
}

export interface NeonDisplayDto extends BaseNonDto {
    name: string
}

