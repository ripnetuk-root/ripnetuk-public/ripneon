// import { InfoList } from '../../app-common/dtos/entity-dtos'
// import { useAppDispatch, useAppSelector } from "../../hooks/redux-hooks";
// import { getApiUrl } from "../../http-client/api-http-client";
// import { fetchTest, setMessage } from "../../store/teststore/teststore-actions";
// import { TestString } from '../../app-common/testcode/testcode'

import NeoGridCell from "./neo-grid-cell"

const x_size = 16
const y_size = 16

const NeoGrid = (): JSX.Element => {
    let rows: JSX.Element[] = []

    for (let y = 0; y < y_size; y++) {
        let cols: JSX.Element[] = []
        for (let x = 0; x < x_size; x++) {
            cols.push(
                <td>
                    <NeoGridCell
                        x={x}
                        y={y}
                    />
                </td>
            )
        }
        rows.push(
            <tr>
                {cols.map(x => x)}
            </tr>
        )
    }

    return (
        <table>
            {rows.map(x => x)}
        </table>
    )
}

export default NeoGrid;