import { BaseNonDto, NeonDisplayDto } from '../../app-frontend/src/app-common/dtos/neon-display-dtos'

class GenericRepo<RepoType extends BaseNonDto> {
    items: RepoType[]
    constructor(items: RepoType[]) {
        this.items = items
    }

    update = (item: RepoType) => {
        for (let i = 0; i < this.items.length; i++) {
            if (this.items[i].id === item.id) {
                this.items[i] = item
                return
            }
        }
    }

    create = (item: RepoType): RepoType => {
        item.id = `ID_${new Date().toLocaleTimeString()}`
        this.items.push(item)
        return item
    }

    delete = (id: string) => {
        this.items = this.items.filter(x => x.id !== id)
    }
}

export const Repo = {
    neonDisplay: new GenericRepo<NeonDisplayDto>([
        {
            id: "RD1",
            name: "Repo Display 1"
        }
    ])
}
