export interface InfoList {
    title: string
    items: string[]
}

export interface TestDto {
    infoLists: InfoList[]
    testMessage: string
}
