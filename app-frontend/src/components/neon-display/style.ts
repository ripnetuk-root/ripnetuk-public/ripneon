import { css } from '@emotion/react'

export function getStyles() {
  return css`
    .idoxDebugItem {
      border-style: solid;
      border-width: 4px;
      padding: 4px;
      margin: 4px;
    }

    .idoxDebugInputAndCanned
    {
      border-style: solid;
      border-width: 1px;
      padding: 4px;
      margin: 4px;
    }

    .idoxDebugFloatToggleButton
    {
      width: 100%;
      height: 100%;
      background-color: #808020;
    }
  `
}
