import { createSlice, PayloadAction, createAsyncThunk } from "@reduxjs/toolkit";
import { NeonDisplayDto } from '../app-common/dtos/neon-display-dtos'
import ApiHttpClient from "../http-client/api-http-client";

export interface NeonDisplayStoreState {
    items: NeonDisplayDto[] | undefined
    selectedItem: NeonDisplayDto | undefined
}

const initialStoreState: NeonDisplayStoreState = {
    items: undefined,
    selectedItem: undefined
}

const neonDisplayStoreSlice = createSlice({
    name: 'neondisplay',
    initialState: initialStoreState,
    reducers: {
        setItems(state, action: PayloadAction<NeonDisplayDto[]>) {
            state.items = action.payload
        },
        setItem(state, action: PayloadAction<NeonDisplayDto>) {
            if (!state.items) {
                state.items = [action.payload]
                return
            }

            const existing = state.items.filter(x => x.id === action.payload.id)
            if (existing.length > 0) {
                const idx = state.items.indexOf(existing[0])
                state.items[idx] = action.payload
            } else {
                state.items.push(action.payload)
            }
        },
        deleteItem(state, action: PayloadAction<NeonDisplayDto>) {
            if (!state.items) {
                return
            }
            state.items = state.items.filter(x => x.id !== action.payload.id)
        },
        setSelectedItem(state, action: PayloadAction<NeonDisplayDto>) {
            state.selectedItem = action.payload
        }
    }
})

export const { setItems, setItem, deleteItem, setSelectedItem } = neonDisplayStoreSlice.actions
export default neonDisplayStoreSlice.reducer;

export const backendGetDisplays = createAsyncThunk('neon-display/fetchNeonDisplays', async (_, thunkAPI) => {
    const res = await await ApiHttpClient().get<NeonDisplayDto[]>("/neondisplay")
    thunkAPI.dispatch(setItems(res.data))
})

export const backendPutDisplay = createAsyncThunk('neon-display/pushNeonDisplay', async (item: NeonDisplayDto, thunkAPI) => {
    await ApiHttpClient().put(`/neondisplay/${item.id}`, item)
    thunkAPI.dispatch(setItem(item))
})

export const backendPostDisplay = createAsyncThunk('neon-display/pushNeonDisplay', async (item: NeonDisplayDto, thunkAPI) => {
    const res = await ApiHttpClient().post<NeonDisplayDto>(`/neondisplay/${item.id}`, item)
    thunkAPI.dispatch(setItem(res.data))
})

export const backendDeleteDisplay = createAsyncThunk('neon-display/pushNeonDisplay', async (item: NeonDisplayDto, thunkAPI) => {
    await ApiHttpClient().delete<NeonDisplayDto>(`/neondisplay/${item.id}`)
    thunkAPI.dispatch(deleteItem(item))
})