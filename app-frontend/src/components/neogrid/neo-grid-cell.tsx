// import { InfoList } from '../../app-common/dtos/entity-dtos'
// import { useAppDispatch, useAppSelector } from "../../hooks/redux-hooks";
// import { getApiUrl } from "../../http-client/api-http-client";
// import { fetchTest, setMessage } from "../../store/teststore/teststore-actions";
// import { TestString } from '../../app-common/testcode/testcode'

interface Props {
    x: number
    y: number
}

const NeoGridCell = (props: Props): JSX.Element => {
    const col = "red"

    return (
        <p
            style={{ backgroundColor: col }}
        >
            {props.x},{props.y}
        </p>
    )
}

export default NeoGridCell;